﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace spook.tube
{
    /**
     * local webserver to allow token exchanging with the host
     */
    internal class HTTPServer
    {
        Thread thread;
        HttpListener listener;
        private int port;

        public HTTPServer(int port) {
            this.port = port;
        }

        public void Start()
        {
            thread = new Thread(() => WorkerTask());
            thread.Start();
        }

        void WorkerTask()
        {
            listener = new HttpListener();
            listener.Prefixes.Add($"http://localhost:{this.port}/");
            listener.Start();

            Plugin.Logger.LogInfo($"Listening on port :{this.port}");

            while (true)
            {
                HttpListenerContext ctx = listener.GetContext();
                var req = ctx.Request;
                using HttpListenerResponse resp = ctx.Response;

                Plugin.Logger.LogInfo("[Webserver] Handling request for " + req.HttpMethod + " : " + req.Url + " : " + req.UserAgent);

                resp.Headers.Set("Access-Control-Allow-Origin", "*");
                resp.Headers.Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
                resp.StatusCode = (int)HttpStatusCode.OK;
                resp.StatusDescription = "status ok";

                switch (req.HttpMethod)
                {
                    case "POST":
                        {
                            string token;
                            using (var reader = new StreamReader(req.InputStream, req.ContentEncoding))
                            {
                                token = reader.ReadToEnd();
                            }

                            Plugin.Logger.LogInfo("Received token from HTTP server, forwarding to uploader...");
                            Plugin.uploader.HandleLogin(token); // pass token to uploader
                            break;
                        }
                    default:
                        {
                            resp.Headers.Set("Content-Type", "text/plain");
                            string data = "spook.tube Content Warning Mod server";
                            byte[] buffer = Encoding.UTF8.GetBytes(data);
                            resp.ContentLength64 = buffer.Length;

                            using Stream ros = resp.OutputStream;
                            ros.Write(buffer, 0, buffer.Length);
                            break;
                        }
                }

                resp.Close();
            }
        }
    }
}
