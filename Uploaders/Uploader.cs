﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace spook.tube.Uploaders
{
    public abstract class Uploader
    {
        public abstract bool IsAuthenticated();
        public abstract void OpenAuth();
        public abstract void HandleLogin(string token);
        public abstract Task Upload(IPlayableVideo video, VideoMeta meta);
    }

    public class VideoMeta
    {
        public List<LoadedPlugin> Plugins;
        public List<ContentPlayer> Players;
    }
}
