﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Policy;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;

namespace spook.tube.Uploaders
{
    /**
     * PeerTube uploader
     */
    public class PTUploader : Uploader
    {
        private string hostname;
        private static string token;
        private HttpClient client;
        private static int channelId;

        public PTUploader(string hostname)
        {
            this.hostname = hostname;

            client = new HttpClient();
        }

        public override bool IsAuthenticated()
        {
            return token != null;
        }

        private HttpRequestMessage buildRequest(string endpoint, HttpMethod method)
        {
            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri(hostname + endpoint),
                Method = method
            };
            request.Headers.Add("Authorization", "Bearer " + token);

            return request;
        }

        private string getVideoURL(string shortUUID)
        {
            return $"{hostname}/videos/update/{shortUUID}?ref=mod-upload";
        }

        public override void OpenAuth()
        {
            Application.OpenURL(hostname + "/p/mod/token");
        }

        public override void HandleLogin(string token)
        {
            PTUploader.token = token;
            var response = client.SendAsync(buildRequest("/api/v1/users/me", HttpMethod.Get)).Result;
            var result = response.Content.ReadAsStringAsync().Result;

            Plugin.Logger.LogInfo("Token check result: " + result);

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var json = JsonConvert.DeserializeObject<PTAccount>(result);
                Plugin.Logger.LogInfo("Logged in as " + json.username);

                if (json.videoChannels.Length == 0)
                {
                    Plugin.Logger.LogError("User has no channels? Cannot load.");
                    return;
                }

                var channel = json.videoChannels[0];
                channelId = channel.id;
                Plugin.Logger.LogInfo($"Using channel {channel.displayName} ({channel.id})");

                Plugin.onFinishAuth?.Invoke();
            }
            else
            {
                Plugin.Logger.LogWarning("Failed to login");
            }
        }

        public override Task Upload(IPlayableVideo recording, VideoMeta meta)
        {
            recording.TryGetVideoPath(out string path);
            return Task.Factory.StartNew(() => UploadThread(path, meta));
        }

        void UploadThread(string path, VideoMeta meta)
        {
            Plugin.Logger.LogInfo("Starting upload of " + path);
            Plugin.Logger.LogInfo("File name: " + Path.GetFileName(path));

            var mime = Plugin.getVideoMime(path);
            Plugin.Logger.LogInfo("File mime type: " + mime);

            var videoName = "In-Game Upload";

            if (mime == null)
            {
                Plugin.Logger.LogWarning("I don't know what mime type the video is (" + Path.GetExtension(path) + ")");
                return;
            }

            try
            {
                var content = new MultipartFormDataContent();
                content.Add(new StringContent(channelId + ""), "channelId");
                content.Add(new StringContent(videoName), "name");
                content.Add(new StringContent("true"), "usingMod");
                if (Plugin.modpackID != null)
                {
                    Plugin.Logger.LogWarning($"Uploading video using modpackId {Plugin.modpackID}");
                    content.Add(new StringContent(Plugin.modpackID), "modpackId");
                }
                content.Add(new StringContent(JsonConvert.SerializeObject(meta.Plugins)), "mods");
                content.Add(new StringContent(JsonConvert.SerializeObject(meta.Players)), "players");

                var fileContent = new ByteArrayContent(File.ReadAllBytes(path));
                fileContent.Headers.ContentType = MediaTypeHeaderValue.Parse(mime);
                content.Add(fileContent, "videofile", Path.GetFileName(path));

                var request = buildRequest("/api/v1/videos/upload", HttpMethod.Post);
                request.Content = content;

                var response = client.SendAsync(request).Result;
                var result = response.Content.ReadAsStringAsync().Result;

                Plugin.Logger.LogInfo("Upload response: " + result);

                switch (response.StatusCode)
                {
                    case System.Net.HttpStatusCode.OK:
                        var json = JsonConvert.DeserializeObject<UploadResult_Success>(result);
                        Plugin.Logger.LogInfo("Successfully uploaded " + json.video.shortUUID);

                        Application.OpenURL(getVideoURL(json.video.shortUUID));
                        break;

                    default:
                        Plugin.Logger.LogError("Error while uploading " + response.StatusCode + " : " + result);
                        break;
                }
            }
            catch (Exception e)
            {
                Plugin.Logger.LogError(e);
            }
        }

        private class UploadResult_Success
        {
            public Video video;

            public class Video
            {
                public int id;
                public string shortUUID;
                public string uuid;
            }
        }

        private class PTAccount
        {
            public string username;
            public PTChannel[] videoChannels;
        }

        private class ChannelResponse
        {
            public int total;
            public PTChannel[] data;
        }

        private class PTChannel
        {
            public int id;
            public string displayName;
        }
    }
}
