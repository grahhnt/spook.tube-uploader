# 1.1.1

- Add LimeWarningModpack to README

# 1.1.0

- Fix bug with authentication
- Upload players & mods to the server
- Add a new button to the TV

# 1.0.5

- Add info banner when mod prompts to edit video

# 1.0.4

- Add debug line
- Flag videos when uploading as coming from the mod

# 1.0.3

- Allow non-host players to upload to spook.tube
- Add modpack support 🎉

# 1.0.0

- Initial release
- Big thank you to [RedBigz](https://redbigz.com/) for making the original mod
