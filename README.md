<div align="center">
    <img src="https://cdn.sc07.company/logos/spook.tube/logov2.png" alt="spook.tube" height="100">
    <br /><br />
    <img alt="Thunderstore Downloads" src="https://img.shields.io/thunderstore/dt/sc07/spooktube?style=for-the-badge&logo=thunderstore&logoColor=white&labelColor=black&color=white">
    <a href="https://gitlab.com/grahhnt/spook.tube-uploader"><img alt="Static Badge" src="https://img.shields.io/badge/Git_Repo-red?style=for-the-badge&logo=git&logoColor=white"></a>
    <a href="https://discord.gg/36grxaS6Vq"><img alt="Static Badge" src="https://img.shields.io/badge/Join_The_Discord-%237289DA?style=for-the-badge&logo=discord&logoColor=white"></a>
    <a href="https://spook.tube"><img alt="Static Badge" src="https://img.shields.io/badge/Watch_Videos-black?style=for-the-badge"></a>
</div>

# spook.tube Uploader

<img src="https://cdn.sc07.company/spook.tube/upload-to-spooktube.png" align="right" width="300">

### Upload your videos to [spook.tube](https://spook.tube)!

This mod adds a new button to the TV that allows you to upload your video to [spook.tube](https://spook.tube)

## Website

<a href="https://spook.tube/videos/trending">
  <img src="https://cdn.sc07.company/spook.tube/website-trending.png" width="500" alt="Screenshot of spook.tube trending page">
</a>

If you have edited footage, you can upload that directly on the website

## Video Metadata

As of 1.1.0, your player faces & loaded mods will be attached to your videos

There's plans to list mods on the website, if you are a mod dev [join the Discord](https://discord.gg/36grxaS6Vq)

## Compatibility

This mod should work with _every_ mod 🎉🎉🎉

If you experience compatibility issues, [join the Discord](https://discord.gg/36grxaS6Vq)

This is a client-side mod, and therefore doesn't require anyone else to have the mod

## Video Approval

This video does not scan your uploads automatically.

Every video submitted has to get approved by the site moderators, which on average is under 4 hours, typically within an hour.

[Join the Discord](https://discord.gg/36grxaS6Vq) to get video approval notifications.

### Quota

By default, new users are given 50mb of video uploads (each non-modded Content Warning clip is ~5mb). [Join the Discord](https://discord.gg/36grxaS6Vq) if you have questions!

## How it works

⚠ You will need a [spook.tube](https://spook.tube) account to upload videos

When you attempt to upload a video, it will open a browser tab to login to the website, after you login you can upload your videos.

## Modpacks

<img src="https://cdn.sc07.company/spook.tube/modpack.png" align="right" width="200">

Modpacks using this mod will get their modpack automatically added to the description of videos 🎉

- [Content Expansion](https://lethal-extended.com/content-expansion)
- [LimeWarningModpack](https://thunderstore.io/c/lethal-company/p/LimeSkillZ/LimeWarningModpack/)
- _Yours?_ [Join the Discord](https://discord.gg/36grxaS6Vq)

## Credits

This mod is a fork of [SpookTubeEX by RedBigz](https://thunderstore.io/c/content-warning/p/RedBigz/SpookTubeEX/), for additional credits visit that mod's page
