﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace spook.tube
{
    internal static class Utils
    {
        // https://discussions.unity.com/t/how-to-find-an-inactive-game-object/129521
        public static GameObject FindObject(this GameObject parent, string name)
        {
            Transform[] trs = parent.GetComponentsInChildren<Transform>(true);
            foreach (Transform t in trs)
            {
                if (t.name.StartsWith(name))
                {
                    return t.gameObject;
                }
            }
            return null;
        }

        public static bool DoesObjectExist(this GameObject parent, string name)
        {
            return FindObject(parent, name) != null;
        }
    }
}
