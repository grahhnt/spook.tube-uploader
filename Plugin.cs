﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Security.Cryptography;
using BepInEx;
using BepInEx.Bootstrap;
using BepInEx.Configuration;
using BepInEx.Logging;
using HarmonyLib;
using Newtonsoft.Json;
using Photon.Pun;
using spook.tube;
using spook.tube.Uploaders;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.UIElements.Collections;
using static TMPro.SpriteAssetUtilities.TexturePacker_JsonArray;

namespace spook.tube
{
    [BepInPlugin(PluginInfo.PLUGIN_GUID, PluginInfo.PLUGIN_NAME, PluginInfo.PLUGIN_VERSION)]
    [ContentWarningPlugin(PluginInfo.PLUGIN_GUID, PluginInfo.PLUGIN_NAME, true)]
    public class Plugin : BaseUnityPlugin
    {
        Harmony harmony;

        //public static ConfigEntry<bool> IncludeSteamIDs;
        public static ConfigEntry<string> URL;
        public static Uploader uploader;
        public static string modpackID;

        public static Action onFinishAuth;

        internal new static ManualLogSource Logger;

        private void Awake()
        {
            Logger = base.Logger;
            // Config
            URL = Config.Bind("Networking", "spook.tube Host", "https://spook.tube", "A PeerTube-compatible instance");
            //IncludeSteamIDs = Config.Bind("Privacy", "Include Steam IDs", true, "Include Steam IDs when uploading to spook.tube");

            LoadModpackID();

            // Plugin startup logic
            Logger.LogInfo($"spook.tube has loaded.");
            if (modpackID != null)
                Logger.LogWarning($"Using modpack ID: {modpackID}");

            // Create patches
            harmony = new("tube.spook.mod");
            harmony.PatchAll();

            uploader = new PTUploader(URL.Value);

            // this port number needs to match the server's
            HTTPServer server = new(48923);
            server.Start();
        }

        private void OnDestroy()
        {
            harmony.UnpatchSelf();

            Logger.LogInfo($"spook.tube has unloaded."); // Unpatch everything
        }

        public static string getVideoMime(string path)
        {
            var extensions = new Dictionary<string, string>();
            extensions.Add(".webm", "video/webm");
            extensions.Add(".mp4", "video/mp4");

            string ext = Path.GetExtension(path);
            if (extensions.ContainsKey(ext))
            {
                return extensions.Get(ext);
            }
            else
            {
                return null;
            }
        }

        public void LoadModpackID()
        {
            List<string> folders = Directory.GetDirectories(Paths.PluginPath, "SpookTube", SearchOption.AllDirectories).ToList();

            foreach (string spooktubeFolder in folders)
            {
                var path = Path.Combine(spooktubeFolder, "modpackid.txt");
                if (File.Exists(path))
                    modpackID = File.ReadAllText(path);
            }
        }

        public static List<LoadedPlugin> GetLoadedPlugins()
        {
            List<LoadedPlugin> loadedPlugins = new List<LoadedPlugin>();

            foreach(var plugin in ThunderstoreAPI.Library.GetLoadedMods())
            {
                loadedPlugins.Add(new LoadedPlugin()
                {
                    ThunderstoreName = plugin.name,
                    Version = $"{plugin.versionNumber.major}.{plugin.versionNumber.minor}.{plugin.versionNumber.patch}"
                });
            }

            return loadedPlugins;
        }

        public static List<ContentPlayer> GetPlayers()
        {
            //bool includeSteamIDs = IncludeSteamIDs.Value;
            List<ContentPlayer> contentPlayers = new();
            PlayerVisor[] players = GameObject.FindObjectsOfType<PlayerVisor>();

            /*if (includeSteamIDs)
            {
                Logger.LogInfo("Including Steam IDs in player list");
            } else
            {
                Logger.LogInfo("Not including Steam IDs in player list (disabled in config)");
            }*/

            foreach (var player in players)
            {
                PhotonView user = player.gameObject.GetComponent<PhotonView>();

                ContentPlayer cplayer = new()
                {
                    FaceText = player.visorFaceText.text,
                    FaceColor = ColorUtility.ToHtmlStringRGB(player.visorColor.Value),
                    FaceRotation = (int)player.FaceRotation
                };

                //if (includeSteamIDs)
                //{
                //    cplayer.SteamID = user.Owner.UserId;
                //}

                contentPlayers.Add(cplayer);
            }

            return contentPlayers;
        }

        /**
         * Create upload button on the TV
         * 
         * This is based off of the replay button, cloning, resizing, positioning
         */
        public static void CreateUploadButton()
        {
            if (Utils.DoesObjectExist(GameObject.Find("ShowVideoState/VIDEO/VideoDone"), "STUpload"))
            {
                // the button already exists, do not create a new one
                return;
            }

            GameObject replayButton = GameObject.Find("VideoDone/Replay");

            // create the button, based off of the replay button
            GameObject newBtn = GameObject.Instantiate(replayButton);
            newBtn.name = "STUpload";
            newBtn.transform.SetParent(replayButton.transform.parent, true);
            newBtn.transform.localPosition = new Vector3(0, -125, 0);
            newBtn.transform.rotation = replayButton.transform.rotation;
            newBtn.transform.localScale = new Vector3(1, 1, 1);

            // change the image to the save icon
            // TODO: maybe change to a cloud icon?
            GameObject image = GameObject.Find("STUpload/Image");
            image.transform.localPosition = new Vector3(-100, 0, 0);
            image.transform.rotation = replayButton.transform.rotation;
            image.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            Sprite imageSprite = GameObject.Find("VideoDone/SaveVideo/Image").GetComponent<Image>().sprite;
            image.GetComponent<Image>().sprite = imageSprite;

            // prepare the text
            GameObject text = GameObject.Find("STUpload/Text (TMP)");
            text.transform.localPosition = new Vector3(0, 0, 0);
            text.transform.rotation = replayButton.transform.rotation;
            text.transform.localScale = new Vector3(1, 1, 1);

            text.GetComponent<UnityEngine.Localization.PropertyVariants.GameObjectLocalizer>()
                .enabled = false;

            // set the text of the button (with translation disabled)
            TextMeshProUGUI textComp = text.GetComponent<TMPro.TextMeshProUGUI>();
            textComp.SetText("UPLOAD TO SPOOK.TUBE");

            // create the interaction point

            // we have to use the utils because the interaction starts off disabled at this point
            // GameObject.Find only works if the object is active
            GameObject replay = Utils.FindObject(GameObject.Find("UploadMachine2"), "ReplayInt");
            GameObject newInt = GameObject.Instantiate(replay);
            newInt.name = "STUploadInt";

            // replace the exec target
            GameObject.Destroy(newInt.GetComponent<ReplayVideoInteractable>());
            newInt.AddComponent<UploadButton>();

            // position it to match where it is
            newInt.transform.SetParent(replay.transform.parent, true);
            newInt.transform.position = newBtn.transform.position;
            newInt.transform.rotation = newBtn.transform.rotation;
            newInt.transform.localScale = new Vector3(1, 1, 1);
        }

        public static void StartUpload()
        {
            GameObject showVideoState = Utils.FindObject(GameObject.Find("McScreen/Content"), "ShowVideoState");
            if(showVideoState == null)
            {
                Logger.LogError("Failed to get the ShowVideoState object");
                return;
            }

            UploadCompleteUI uploadui = showVideoState.GetComponent<UploadCompleteUI>();
            if(uploadui == null)
            {
                Logger.LogError("Failed to get UploadCompleteUI");
                return;
            }

            IPlayableVideo video = new Traverse(uploadui).Field("m_replayVideo").GetValue() as IPlayableVideo;
            GameObject States = Utils.FindObject(GameObject.Find("UploadMachine2/McScreen"), "Content");

            GameObject ShowVideoState = Utils.FindObject(States, "ShowVideoState");
            GameObject UploadingState = Utils.FindObject(States, "UploadingState");

            // Show Uploading Screen
            UploadingState.SetActive(true);
            ShowVideoState.SetActive(false);

            VideoMeta meta = new()
            {
                Plugins = GetLoadedPlugins(),
                Players = GetPlayers()
            };

            uploader.Upload(video, meta).ContinueWith((t) =>
            {
                UploadingState.SetActive(false);

                UploadVideoStationStateMachine stateMachine = new Traverse(GameObject.Find("/Tools/UploadMachine2").GetComponent<UploadVideoStation>()).Field("m_stateMachine").GetValue() as UploadVideoStationStateMachine;
                stateMachine.SwitchState<UploadVideoState>();
            });
        }

        public static void WaitForLogin(Action onFinish)
        {
            onFinishAuth = onFinish;
            Plugin.uploader.OpenAuth();
        }
    }

    public class LoadedPlugin
    {
        public string ThunderstoreName; // eg sc07-spooktube
        public string Version;
    }

    public class ContentPlayer
    {
        public string SteamID;
        public string FaceText;
        public string FaceColor; // hex
        public int FaceRotation;
    }

    class UploadButton : Interactable
    {
        public UploadVideoStation UploadVideoStation;

        private void Start()
        {
            // not sure if this is needed, but might as well add it
            // i think it might be used in accessibility
            this.hoverText = "Upload to spook.tube";
        }

        public override void Interact(Player player)
        {
            Plugin.Logger.LogWarning("Upload to spook.tube button has been pressed");

            if(Plugin.uploader.IsAuthenticated())
            {
                Plugin.StartUpload();
            }
            else
            {
                Plugin.uploader.OpenAuth();
            }
        }

        public override void OnStartHover(Player player)
        {
            // for some reason the base class does not have these methods
        }

        public override void OnEndHover(Player player)
        {
            // for some reason the base class does not have these methods
        }
    }
}

[HarmonyPatch(typeof(UploadCompleteUI), nameof(UploadCompleteUI.PlayVideo))]
class VideoEndedUIInjection
{
    [HarmonyPostfix]
    static void Postfix(UploadCompleteUI __instance)
    {
        Plugin.CreateUploadButton();
    }
}